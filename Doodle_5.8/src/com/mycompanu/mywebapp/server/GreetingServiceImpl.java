package com.mycompanu.mywebapp.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.*;

import com.mycompanu.mywebapp.client.Evento;
import com.mycompanu.mywebapp.client.GreetingService;
import com.mycompanu.mywebapp.client.Utente;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;



@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService
{
	private Scanner scanner;
    DB db = DBMaker.newFileDB(new File("doodle")).make();
	Map<Integer,Evento> mapEvento = db.getTreeMap("Eventi");


	  /**
	   * Escape an html string. Escaping data received from the client helps to
	   * prevent cross-site script vulnerabilities.
	   * 
	   * @param html the html string to escape
	   * @return the escaped string
	   */
	/*
		private String escapeHtml(String html)
		{
			if (html == null)
			{
				return null;
	    	}
			return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		}

*/

	public String inserisciUtente (String input) throws IllegalArgumentException
	{
		DB db = DBMaker.newFileDB(new File("doodle")).closeOnJvmShutdown().make();
		
		/*
		 * Creo le mappe di cui avr� bisogno:
		 * UserInfo - infromazioni dell utente
		 * UserId - <IdUtente><Username>
		 */
		
	    ConcurrentNavigableMap<String,String> listaUtenti = db.getTreeMap("ListaUtenti");
	    
	    /*
	     * leggiamo i dati inseriti in input
	     */
	    
	    scanner = new Scanner(input);
		Scanner scan = scanner.useDelimiter("\\s*\\,\\s*");
	    String nome = scan.next();
	    String username = scan.next();
	    String email = scan.next();
	    String password = scan.next();
	    
	    /*
	     * prepariamo i dati che andremo ad inviare
	     */
	    
	    System.out.println(listaUtenti.containsKey(username));
	    System.out.println(listaUtenti.containsValue(email));
	    System.out.println(listaUtenti.containsKey(username)||listaUtenti.containsValue(email));
	    if(!(listaUtenti.containsKey(username)||listaUtenti.containsValue(email)))
	    {
	    	System.out.println("********** Utente non presente **********");

		    ConcurrentNavigableMap<String,Utente> mapUtenti = db.getTreeMap("Users");
		    ConcurrentNavigableMap<String,String> mapPass = db.getTreeMap("UsersPass");
		    
	    	Utente user = new Utente(nome,username,email, password);
	    	
	    	listaUtenti.put(username,email);
	    	mapUtenti.put(username,user);
	    	mapPass.put(username,password);
	    	
	    	System.out.println("stampiamo la mappa degli utenti");
		    System.out.println(mapUtenti);
	    	System.out.println("stampiamo la lista di utenti - email");
		    System.out.println(listaUtenti);
		    System.out.println("lista utente - password");
		    System.out.println(mapPass);
	    }
	    else
	    {
	    	username =username+"X";
	    	System.out.println("Funziona ed esiste gi�");
	    }	    
	    db.commit();
	    scan.close();
	    db.close();
		return username;
	}
	


	public String controlloUsername(String name) throws IllegalArgumentException
	{
		System.out.println("stringa in arrivo " + name);
		scanner = new Scanner(name);
		Scanner scan = scanner.useDelimiter("\\s*\\,\\s*");
		String username = scan.next();
		String password = scan.next();
		DB db = DBMaker.newFileDB(new File("doodle")).make();
		
		ConcurrentNavigableMap<String,String> mapId = db.getTreeMap("UsersPass");
		
		System.out.println(mapId);

		if(mapId.containsKey(username)&&mapId.containsValue(password))
		{
			System.out.println("trovato");
			return username;
		}
		else
		{
			System.out.println("non trovato");
		}
		scan.close();
		return username+"X";
	}



	public String inserisciEvento(String evento) throws IllegalArgumentException
	{
		System.out.println(evento);
		scanner = new Scanner(evento);
		Scanner scanD = scanner.useDelimiter("\\s*\\-\\s*");
		
		Scanner scanT;
		String dati = scanD.next();
		scanT = new Scanner(dati);
		Scanner scan = scanT.useDelimiter("\\s*\\,\\s*");
		DB dbcd = DBMaker.newFileDB(new File("doodleEv")).closeOnJvmShutdown().make();
		
		ConcurrentNavigableMap<Integer,Evento> mapEvento = dbcd.getTreeMap("eventiP");
		ConcurrentNavigableMap<String,String> mapUtenteEv = dbcd.getTreeMap("utenteEvento");
		
		String nome = scanT.next();
		String luogo = scanT.next();
		String descrizione = scanT.next();
		String username = scanT.next();
		scan.close();
		scanT.close();
		
		System.out.println(nome);
		System.out.println(luogo);
		System.out.println(descrizione);
		System.out.println(username);
		
		String sondaggiA = scanD.next();
		Scanner scanner2 = new Scanner(sondaggiA);
		Scanner sond = scanner2.useDelimiter("\\s*,\\s*");
		ArrayList<String> tempS = new ArrayList<String>();
		
		while (sond.hasNext()) 
        {
            tempS.add(sond.next());
        }
		
		sond.close();
		scanner2.close();
		
		String[] s = new String[tempS.size()];
		
		for (int i =0; i< tempS.size();i++)
        {
			System.out.println(tempS.get(i));
			s[i]=(tempS.get(i));
        }
		for (int i =0; i< tempS.size();i++)
        {
			s[i]=(tempS.get(i));
			System.out.println("s" +i+ " inserico - " + tempS.get(i) + " - risulta - " + s[i]);
        }
		for (int i = 0; i < s.length; i++) 
		{
	        System.out.println(s[i]);
	    }
				
		System.out.println("errore1");
		int idEvento = CreaId.creaID("utenteEvento", 2);
		System.out.println(s);
		Evento ev = new Evento(nome,luogo,descrizione,s);
		System.out.println(ev);
		System.out.println("errore2");
		System.out.println(ev.getDate());
				
		String all;

		System.out.println("id - "+idEvento);
		System.out.println("evento - " + ev);
		mapEvento.put(idEvento,ev);
		System.out.println("errore3");
		
		dbcd.commit();
		
		if(mapUtenteEv.containsKey(username))
		{
			System.out.println(mapUtenteEv);
			System.out.println("ringhi");
			all = mapUtenteEv.get(username).toString();
			System.out.println("critical");
			System.out.println("all - " +all);
			all = all + " , " + idEvento;
			mapUtenteEv.put(username,all);
		}
		else
		{
			mapUtenteEv.put(username,idEvento+"");
			System.out.println(mapUtenteEv);
		}
		System.out.println("errore4");
		dbcd.commit();
		//++++++++++++++++++++++++++++++++++
		System.out.println("controllo altra mappa");
		System.out.println(mapEvento);
		System.out.println(mapUtenteEv);

		dbcd.close();
		return username;
	}

	public String controlloEvento(String idEventoS) throws IllegalArgumentException
	{
		int idEvento = Integer.parseInt(idEventoS);
		DB db = DBMaker.newFileDB(new File("doodleEv")).closeOnJvmShutdown().make();

		ConcurrentNavigableMap<Integer,Evento> mapId = db.getTreeMap("eventiP");
		System.out.println(mapId);
		
		if(mapId.containsKey(idEvento))
		{
			return idEvento+"";
		}
		else
		{
			System.out.println("non trovato");
		}
		return "X";
	}

	public String prendiEvento(String idEventoS) throws IllegalArgumentException
	{
		int idEvento = Integer.parseInt(idEventoS);
		DB db = DBMaker.newFileDB(new File("doodleEv")).closeOnJvmShutdown().make();
		ConcurrentNavigableMap<Integer,Evento> mapId = db.getTreeMap("eventiP");

		Evento eventoR = mapId.get(idEvento);
		String[] date = eventoR.getDate();
		
		System.out.println(eventoR);
		
		String big = eventoR.getNomeEvento() + " , " +
					 eventoR.getLuogo() + " , " +
					 eventoR.getDescrizione();
		
		big = big +" - " + date[0];
		for (int i = 1; i < date.length; i++)
		{
	        big = big + " , " + date[i];
	    }
		return big;
	}

	public String[] getDati(String idEventoS) throws IllegalArgumentException
	{
		System.out.println("stringa in arrivo " + idEventoS);
		int idEvento = Integer.parseInt(idEventoS);
		DB db = DBMaker.newFileDB(new File("doodleEv")).closeOnJvmShutdown().make();
		
		ConcurrentNavigableMap<Integer,Evento> mapId = db.getTreeMap("eventiP");
		System.out.println(mapId);
		Evento eventoR = mapId.get(idEvento);
		String[] date = eventoR.getDate();

		String[] allD = new String[date.length+4];
		
		allD[0]=eventoR.getNomeEvento();
		allD[1]=eventoR.getLuogo();
		allD[2]=eventoR.getDescrizione();
		allD[3]=idEventoS;	
		
		for (int i = 4; i < allD.length; i++)
		{
	        allD[i] = date[i-4];
	    }
		return allD;
	}

	public String vota (String all)
	{
		DB dbsond = DBMaker.newFileDB(new File("doodleSond")).closeOnJvmShutdown().make();
		ConcurrentNavigableMap<String,String> mapSondaggi = dbsond.getTreeMap("Sondaggi");
		Scanner scan = new Scanner(all);
		scan.useDelimiter("\\s*\\,\\s*");
		String sondaggio = scan.next();
		System.out.println(sondaggio);
		System.out.println(mapSondaggi);
		String votanti=null;
		if(mapSondaggi.get(sondaggio)==null)
		{
			mapSondaggi.put(sondaggio, scan.next());
			System.out.println("DENTRO L'IF!  " + mapSondaggi);
		}
		else
		{
			votanti=mapSondaggi.get(sondaggio);
			while(scan.hasNext())
			{
				String utente = scan.next();
				String confronto = mapSondaggi.get(utente);
				System.out.println("ORA STAMPO CONFRONTO " + confronto);
				if(confronto!=null)
				{
					System.out.println("**************************");
					System.out.println("L'utente ha gi� votato");
					break;
				}
				votanti= votanti + " , " + utente;
				System.out.println("dentro l'else " + votanti);
			}
			mapSondaggi.put(sondaggio, votanti);
		}
		dbsond.commit();
		dbsond.close();
		return null;
	}
	
	public String[] aggiungiCommenti (String[] commenti)
	{
		DB dbcomm = DBMaker.newFileDB(new File("doodlecomm")).closeOnJvmShutdown().make();
		ConcurrentNavigableMap<String,String> mapCommenti = dbcomm.getTreeMap("Commenti");
		String utente = commenti[0];
		String commento = commenti[1];
		String idEvento = commenti[2];
		String toInsert = utente + " , " + commento;
		String inserimento =null;
		System.out.println("!!!!!!!!!! sono in aggiungiCommenti !!!!!!!");
		System.out.println("ora stampo l'username: " + utente);
		System.out.println("ora stampo il commento: " + commento);
		System.out.println("ora stampo l'idEvento: " + idEvento);
		System.out.println("ora stampo l'insert: " + toInsert);

		String confronto = mapCommenti.get(idEvento);
		if(confronto!=null);
		{
			System.out.println("MapCommenti contiene gi� commenti");
			inserimento = mapCommenti.get(idEvento) + " - " + toInsert;
			mapCommenti.put(idEvento, inserimento);
		}
		if(confronto==null)
		{
			System.out.println("primo commento");
			mapCommenti.put(idEvento, toInsert);
		}
		String temp = mapCommenti.get(idEvento);
		System.out.println(temp);
		dbcomm.commit();
		return null;
	}
	

	
	public String[] settaCommenti (String idEvento)
	{
		System.out.println("inizia il settacommenti");
		DB dbcomm = DBMaker.newFileDB(new File("doodlecomm")).closeOnJvmShutdown().make();
		ConcurrentNavigableMap<String,String> mapCommenti = dbcomm.getTreeMap("Commenti");
		System.out.println("doodlecomm aperto");
		ArrayList<String> contatore = new ArrayList<String>();
		String scomponi = mapCommenti.get(idEvento);
		System.out.println("Stringa da scomporre: " + scomponi);
		
		scanner = new Scanner(scomponi);
		Scanner scanD = scanner.useDelimiter("\\s*\\-\\s*");

		System.out.println("STO SETTANDO I COMMENTI");
		int s=0;
		while(scanD.hasNext())
		{
			System.out.println("GIRO: " +s);
			String commento = scanD.next();
			contatore.add(commento);
			System.out.println("Contatore vale: " +contatore.get(s));
			s++;
		}
		String[] ritorno = new String[contatore.size()];
		for(int i=0; i< contatore.size(); i++)
		{
			ritorno[i]=contatore.get(i);
			System.out.println(ritorno[i]);
		}
		return ritorno;
	}
	
	
	public String[] creaMapSondaggi (String[] sondaggi) throws IllegalArgumentException
	{
		String [] numeroVoti = new String[sondaggi.length];
		System.out.println("********** ORA SALVO IL NUMERO DEI VOTI **********");
		DB dbsond = DBMaker.newFileDB(new File("doodleSond")).closeOnJvmShutdown().make();
		ConcurrentNavigableMap<String,String> mapSondaggi = dbsond.getTreeMap("Sondaggi");
		for (int i=0; i < sondaggi.length; i++)
		{
			numeroVoti[i] = mapSondaggi.get(sondaggi[i]);
			System.out.println(numeroVoti[i]);
		}
		for(int i =0; i<sondaggi.length; i++)
		{
			numeroVoti[i]= "" + getNumVoti(numeroVoti[i]);
		}
		System.out.println("*************** STAMPO MAPsONDAGGI *******");
		System.out.println(mapSondaggi);
		return numeroVoti;
	}	
	
	int getNumVoti(String elenco)
	{
		int n=0;
		Scanner scanTemp = new Scanner(elenco);
		scanTemp.useDelimiter("\\s*\\,\\s*");
		while(scanTemp.hasNext())
		{
			scanTemp.next();
			n++;
		}
		scanTemp.close();
		return n;
	}

	public String[] idEventi (String username)throws IllegalArgumentException
	{
		DB db = DBMaker.newFileDB(new File ("doodleEv")).closeOnJvmShutdown().make();

		ConcurrentNavigableMap<String,String> mapUtenteEv = db.getTreeMap("utenteEvento");
		String listaID;
		listaID = mapUtenteEv.get(username);
		
		scanner = new Scanner(listaID);
		Scanner scan = scanner.useDelimiter("\\s*,\\s*");
		ArrayList<String> temp = new ArrayList<String>();
		while(scan.hasNext())
		{
			temp.add(scan.next());
		}
		String[] matt = new String[temp.size()];
		for(int i = 0; i< temp.size(); i++)
		{
			matt[i] = temp.get(i).toString();
		}
		return matt;
	}
}