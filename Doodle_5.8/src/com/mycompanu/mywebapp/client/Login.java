package com.mycompanu.mywebapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;

public class Login extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);

	@UiField Button login;
	@UiField Button loginNReg;
	@UiField Button registrazione;
	@UiField TextBox textbox_user;
	@UiField TextBox textbox_pass;

	interface LoginUiBinder extends UiBinder<Widget, Login>
	{
	}

	public Login()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiHandler("login")
	void onLoginClick(ClickEvent event)
	{
		String userPass = textbox_user.getText()+" , "+ textbox_pass.getText();
		greetingService.controlloUsername(userPass, new AsyncCallback<String>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("ControlloUsername fallito");
			}
			public void onSuccess(String result)
			{
				if(!result.contains("X"))
				{
					System.out.println("andata bene");
					System.out.println("Utente trovato - "+ result);
					RootPanel root = RootPanel.get();
					GestisciEvento obj = new GestisciEvento();
					obj.setDati(result,2);
					root.clear();
					root.add(obj);
				}
				else
				{
					textbox_pass.setText("USERNAME O PASSWORD ERRATI");
				}
			}
		});
	}
	
	@UiHandler("loginNReg")
	void onLoginNRegClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		PartecipaEvento obj = new PartecipaEvento();
		obj.setDati("Utente non registrato", 0);
		root.clear();
		root.add(obj);
	}

	@UiHandler("registrazione")
	void onRegistrazioneClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		Registrazione obj = new Registrazione();
		root.clear();
		root.add(obj);
	}
}