package com.mycompanu.mywebapp.client;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.AbsolutePanel;

public class InfoEvento extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static InfoEventoUiBinder uiBinder = GWT.create(InfoEventoUiBinder.class);
	
	private String username;
	private int livSic;
	private String[] sondaggi;
	private String idEvento;
	private String nome;
	private String luogo;
	private String descrizione;
	
	@UiField Button partecipa_eventi;
	@UiField Button gestisci_eventi;
	@UiField Button vedi_commenti;
	@UiField Button vai_info_ev2;
	@UiField TextBox ID_evento;
	@UiField TextBox luogo_evento;
	@UiField TextBox ADMIN;
	@UiField TextBox stato;
	@UiField TextBox usernameText;
	@UiField TextBox nomeEvento;
	@UiField AbsolutePanel sondaggio_2;

	interface InfoEventoUiBinder extends UiBinder<Widget, InfoEvento>
	{
	}

	public InfoEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
		ID_evento.setText("Id evento");
		luogo_evento.setText("luogo");
		stato.setText("stato");
	}
	
	public void setDati(String username, int livSic, String idEvento)
	{
		this.username = username;
		this.livSic = livSic;
		this.idEvento = idEvento;
		ID_evento.setText(idEvento+"");
		usernameText.setText(username);
		stato.setText("stato");
		if (livSic==0)
	    {
			gestisci_eventi.setVisible(false);
			vedi_commenti.setVisible(false);
			System.out.println("username non registrato");
			usernameText.setEnabled(true);
	    }
		if(livSic!=3)
		{
			ADMIN.setText("username: ");
		}
		alternativeAsk();
	}
	
	void alternativeAsk()
	{
		greetingService.getDati(idEvento+"", new  AsyncCallback<String[]>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Errore  getDati");
			}
			public void onSuccess(String[] result)
			{
				System.out.println(result);
				superSetAL(result);
			}
		});
	}
	
	void superSetAL(String[] allDati)
	{
		System.out.println(allDati);
		String[] a = allDati;
		nome = a[0];
		luogo = a[1];
		luogo_evento.setText(luogo);
		descrizione = a[2];
		idEvento = a[3];

		nomeEvento.setText(nome + " di: " + username);
		ArrayList<String> date = new ArrayList<String>();
		
		System.out.println(nome);
		System.out.println(luogo);
		System.out.println(descrizione);
		System.out.println(username);
		//******************
		date.clear();
		for(int i=4;i<a.length;i++)
		{
			date.add(a[i]);
			System.out.println("copio: "+ a[i]);
		}
		
		for (int i =0; i< date.size();i++)
        {
			System.out.println(date.get(i));
			System.out.println("stampo data : "+i);
        }
		
		System.out.println("***************");
		for (int i = 0; i < a.length; i++)
		{
	        System.out.println(a[i]);
	    }
		System.out.println("size: " + date.size());
		sondaggi = new String[date.size()];
		for(int i=0; i < date.size(); i++)
		{
			sondaggi[i]= date.get(i);
			System.out.println("sondaggio - " + i + " dato: " + sondaggi[i]);
		}
	}
	
	
	@UiHandler("partecipa_eventi")
	void onPartecipa_eventiClick(ClickEvent event) 
	{
		RootPanel root = RootPanel.get();
		PartecipaEvento obj = new PartecipaEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("gestisci_eventi")
	void onGestisci_eventiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		GestisciEvento obj = new GestisciEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("vedi_commenti")
	void onVedi_commentiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		CommentiEvento obj = new CommentiEvento();
		obj.setDati(username, livSic, idEvento);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("vai_info_ev2")
	void onVai_info_ev2Click(ClickEvent event)
	{
		RootPanel rootPanel = RootPanel.get();
		InfoEvento2 obj = new InfoEvento2();
		obj.setDati(username, livSic, idEvento, nome, luogo, descrizione, sondaggi);
		rootPanel.clear();
		rootPanel.add(obj);
	}
}