package com.mycompanu.mywebapp.client;

import com.google.cloud.sql.jdbc.internal.Exceptions;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;

public class InfoEvento2 extends Composite 
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static InfoEvento2UiBinder uiBinder = GWT.create(InfoEvento2UiBinder.class);
	
	private String username;
	private int livSic;
	private String idEvento;
	private String nome;
	private String luogo;
	private String descrizione;
	private String[] sondaggi;
	private String[] numeroVoti;
	
	@UiField Button indietro;
	@UiField Button vota;
	@UiField Button chiudi_sondaggio;
	@UiField ListBox lista_date;
	@UiField TextBox mostra_orario;
	@UiField TextBox orario;

	interface InfoEvento2UiBinder extends UiBinder<Widget, InfoEvento2>
	{
	}

	public InfoEvento2() 
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	void setDati(String username,int livSic, String idEvento, String nome, String luogo, String descrizione, String[] sondaggi)
	{
		this.username=username;
		this.livSic=livSic;
		this.idEvento=idEvento;
		this.nome=nome;
		this.luogo=luogo;
		this.descrizione=descrizione;
		this.sondaggi=sondaggi;
		orario.setVisible(false);
		if((livSic==1)||(livSic==0)||(livSic==2))
		{
			chiudi_sondaggio.setVisible(false);
			mostra_orario.setVisible(false);
		}
		try
		{
			numeroVoti=setVoti();
			for(int i =0; i< sondaggi.length; i++)
			{
				System.out.println("STAMPO NUMEROVOTI: " + numeroVoti.length);
				System.out.println(numeroVoti[i]);
				lista_date.addItem(sondaggi[i] + numeroVoti[i]);
			}
		}
		catch(Exception e)
		{
			System.out.println("non settati i voti");
			for(int i =0; i< sondaggi.length; i++)
			{
				lista_date.addItem(sondaggi[i]);
			}
		}
	}
	
	@UiHandler("indietro")
	void onIndietroClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		InfoEvento obj = new InfoEvento();
		obj.setDati(username, livSic, idEvento);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("vota")
	void onVotaClick(ClickEvent event)
	{
		int selezionato = lista_date.getSelectedIndex();
		if (selezionato != -1)
		{
			String all = sondaggi[selezionato] + " , " + username;
			greetingService.vota(all, new  AsyncCallback<String>()
			{
				public void onFailure(Throwable caught)
				{
					System.out.println("errore sul voto");
				}
				public void onSuccess(String result)
				{
					System.out.println("Vota ha funzionato");
				}
			});
			RootPanel root = RootPanel.get();
			InfoEvento obj = new InfoEvento();
			obj.setDati(username, livSic, idEvento);
			root.clear();
			root.add(obj);
		}
	}
	
	@UiHandler("chiudi_sondaggio")
	void onChiudi_sondaggioClick(ClickEvent event)
	{
		if((livSic==3))
		{
			try
			{
				greetingService.creaMapSondaggi(sondaggi, new AsyncCallback<String[]>()
				{
					public void onFailure(Throwable caught)
					{
						System.out.println("Fallimento");
					}
					public void onSuccess(String[] result)
					{
						int temp1, temp2, k=0;
						setVoti();
						temp1=Integer.parseInt(numeroVoti[0]);
						for(int i=1; i<numeroVoti.length; i++)
						{
							temp2=Integer.parseInt(numeroVoti[i]);
							if(temp2>temp1)
							{
								temp1=temp2;
								k=i;
							}	
						}
						System.out.println("Il massimo valore e': " + temp1);
						mostra_orario.setText(sondaggi[k] + "   con " + temp1 + " voti");
						vota.setEnabled(false);
						orario.setVisible(true);
					}
				});		
			}
			catch(Exception e)
			{
				System.out.println("chiusura del sondaggio fallita");
			}
		}
	}
	
	String[] setVoti()
	{
		greetingService.creaMapSondaggi(sondaggi, new AsyncCallback<String[]>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("problema sul settaggio del numero di voti");
			}
			
			public void onSuccess(String[] result)
			{
				numeroVoti = result;
				for(int i =0; i < numeroVoti.length; i++)
				{
					System.out.println("Stampo il numero dei voti dell 'evento " +i);
					System.out.println(numeroVoti[i]);
				}
			}
		});
	return numeroVoti;
	}
}