package com.mycompanu.mywebapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;

public class PartecipaEvento extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static PartecipaEventoUiBinder uiBinder = GWT.create(PartecipaEventoUiBinder.class);
	
	private String username;
	private int livSic;
	private String idEvento;
	
	@UiField Button ricerca_ID;
	@UiField Button gestisci_eventi;
	@UiField Button esci;
	@UiField TextBox ID_evento;
	@UiField TextBox usernameText;

	interface PartecipaEventoUiBinder extends UiBinder<Widget, PartecipaEvento>
	{
	}

	public PartecipaEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	void setDati(String username,int livSic)
	{
		this.username = username;
		this.livSic = livSic;
		usernameText.setText(username);
		if (livSic==0)
	    {
			gestisci_eventi.setVisible(false);
			System.out.println("username non registrato");
	    }
	}
	
	@UiHandler("ricerca_ID")
	void onRicerca_IDClick(ClickEvent event)
	{
		System.out.println("ID_evento - "+ ID_evento.getText());
		if(livSic==3)
		{
			livSic--;
		}
		greetingService.controlloEvento(ID_evento.getText(), new  AsyncCallback<String>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Errore in controllo evento");
			}
			public void onSuccess(String result)
			{
				if(!result.contains("X"))
				{
					System.out.println("IdEvento trovato");
					int idEvento1 = Integer.parseInt(ID_evento.getText());
					idEvento = "" + idEvento1;
					RootPanel root = RootPanel.get();
					InfoEvento obj = new InfoEvento();
					System.out.println(idEvento);
					obj.setDati(username, livSic, idEvento);
					root.clear();
					root.add(obj);
				}
				else
				{
					System.out.println("IdNonTrovato");
					ID_evento.setText("Id non presente");
					System.out.println(result);
				}
			}
		});
	}
	
	@UiHandler("gestisci_eventi")
	void onGestisci_eventiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		GestisciEvento obj = new GestisciEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("esci")
	void onEsciClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		Login obj = new Login();
		root.clear();
		root.add(obj);
	}
}