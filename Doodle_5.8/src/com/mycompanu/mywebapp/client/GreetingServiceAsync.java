package com.mycompanu.mywebapp.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync
{
	void controlloUsername (String input, AsyncCallback<String> callback) throws IllegalArgumentException;
	void controlloEvento (String idEvento, AsyncCallback<String> callback) throws IllegalArgumentException;
	void inserisciUtente(String input, AsyncCallback<String> callback) throws IllegalArgumentException;
	void inserisciEvento(String evento,  AsyncCallback<String> callback) throws IllegalArgumentException;
	void prendiEvento (String idEvento, AsyncCallback<String> callback) throws IllegalArgumentException;
	void getDati(String idEvento, AsyncCallback<String[]> callback);
	void vota(String all, AsyncCallback<String> callback);
	void idEventi(String username, AsyncCallback<String[]> callback) throws IllegalArgumentException;
	void creaMapSondaggi(String sondaggi[], AsyncCallback<String[]> callback);
	void aggiungiCommenti(String commenti[], AsyncCallback<String[]> callback);
	void settaCommenti (String idEvento,  AsyncCallback<String[]> callback);
}