package com.mycompanu.mywebapp.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ListBox;
import com.mycompanu.mywebapp.server.GreetingServiceImpl;
import com.google.gwt.user.client.ui.Hidden;

public class GestisciEvento extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private static GestisciEventoUiBinder uiBinder = GWT.create(GestisciEventoUiBinder.class);
	private String username;
	private int livSic;
	String[] elencoId ;
	String idEventoSel;

	@UiField Button crea_evento;
	@UiField Button partecipa_evento;
	@UiField Button apri_evento;
	@UiField TextBox User;
	@UiField ListBox ElencoNomi;

	interface GestisciEventoUiBinder extends UiBinder<Widget, GestisciEvento>
	{
	}

	public GestisciEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	void riempiTabella()
	{
		greetingService.idEventi(username, new  AsyncCallback<String[]>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Elenco non preso");
			}
			public void onSuccess(String[] result)
			{
				elencoId = result;
				for(int k=0; k< elencoId.length; k++)
				{
					greetingService.getDati(elencoId[k], new  AsyncCallback<String[]>()
					{
						public void onFailure(Throwable caught) 
						{
							System.out.println("Problema sul getDati");
						}
						public void onSuccess(String[] result)
						{
							String[] elementi = result;
							{
								System.out.println(elementi[0]);
								String dati = elementi[0];

								int numeroPosizioni = dati.length();
								if (dati.contains(" "))
								{
									numeroPosizioni--;
								}
								while(numeroPosizioni<24)
								{
									dati = dati + "_";
									numeroPosizioni++;
								}
								dati = dati + elementi[1];
								numeroPosizioni= numeroPosizioni + elementi[1].length();
								if (elementi[1].contains(" "))
								{
									numeroPosizioni--;
								}
								while (numeroPosizioni <50)
								{
									dati = dati + "_";
									numeroPosizioni++;
								}
								dati = dati + elementi[3];
								ElencoNomi.addItem(dati);
							}
						}
					});
				}
			}
		});
		for(int j=0;j<elencoId.length;j++)
		{
			System.out.println(elencoId[j]+ "----");
		}
		ElencoNomi.setVisibleItemCount(elencoId.length);
	}
	
	void setDati(String username,int livSic)
	{
		this.username=username;
		this.livSic=livSic;
		User.setText(username);
		try
		{
			riempiTabella();
		}
		catch(Exception e)
		{
			System.out.println("nessun evento");
		}
	}
	
	@UiHandler("crea_evento")
	void onCrea_eventoClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		CreaEvento obj = new CreaEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("partecipa_evento")
	void onPartecipa_eventoClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		PartecipaEvento obj = new PartecipaEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("apri_evento")
	void onApri_eventoClick(ClickEvent event)
	{
		int selezionato = ElencoNomi.getSelectedIndex();
		if(selezionato != -1)
		{
			RootPanel root = RootPanel.get();
			InfoEvento obj = new InfoEvento();
			obj.setDati(username, 3, elencoId[selezionato]);
			root.clear();
			root.add(obj);
		}
	}
}